﻿namespace kino2norm
{
    partial class mainprog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainprog));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button29 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.button33 = new System.Windows.Forms.Button();
            this.button34 = new System.Windows.Forms.Button();
            this.button35 = new System.Windows.Forms.Button();
            this.button36 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.button38 = new System.Windows.Forms.Button();
            this.button39 = new System.Windows.Forms.Button();
            this.button40 = new System.Windows.Forms.Button();
            this.button41 = new System.Windows.Forms.Button();
            this.button42 = new System.Windows.Forms.Button();
            this.button43 = new System.Windows.Forms.Button();
            this.button44 = new System.Windows.Forms.Button();
            this.button45 = new System.Windows.Forms.Button();
            this.button46 = new System.Windows.Forms.Button();
            this.button47 = new System.Windows.Forms.Button();
            this.button48 = new System.Windows.Forms.Button();
            this.button49 = new System.Windows.Forms.Button();
            this.button50 = new System.Windows.Forms.Button();
            this.button51 = new System.Windows.Forms.Button();
            this.button52 = new System.Windows.Forms.Button();
            this.button53 = new System.Windows.Forms.Button();
            this.button54 = new System.Windows.Forms.Button();
            this.button55 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.calendar = new System.Windows.Forms.TextBox();
            this.checkboxdate = new System.Windows.Forms.CheckedListBox();
            this.seattext = new System.Windows.Forms.TextBox();
            this.checkseatbox = new System.Windows.Forms.CheckedListBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(295, -5);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(317, 311);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 28;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click_1);
            // 
            // button29
            // 
            this.button29.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button29.Location = new System.Drawing.Point(525, 97);
            this.button29.Margin = new System.Windows.Forms.Padding(2);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(21, 28);
            this.button29.TabIndex = 45;
            this.button29.Text = "8";
            this.button29.UseVisualStyleBackColor = true;
            // 
            // button30
            // 
            this.button30.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button30.Location = new System.Drawing.Point(500, 97);
            this.button30.Margin = new System.Windows.Forms.Padding(2);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(21, 28);
            this.button30.TabIndex = 44;
            this.button30.Text = "7";
            this.button30.UseVisualStyleBackColor = true;
            // 
            // button31
            // 
            this.button31.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button31.Location = new System.Drawing.Point(475, 97);
            this.button31.Margin = new System.Windows.Forms.Padding(2);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(21, 28);
            this.button31.TabIndex = 43;
            this.button31.Text = "6";
            this.button31.UseVisualStyleBackColor = true;
            // 
            // button32
            // 
            this.button32.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button32.Location = new System.Drawing.Point(449, 97);
            this.button32.Margin = new System.Windows.Forms.Padding(2);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(21, 28);
            this.button32.TabIndex = 42;
            this.button32.Text = "5";
            this.button32.UseVisualStyleBackColor = true;
            this.button32.Click += new System.EventHandler(this.button32_Click);
            // 
            // button33
            // 
            this.button33.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button33.Location = new System.Drawing.Point(423, 97);
            this.button33.Margin = new System.Windows.Forms.Padding(2);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(21, 28);
            this.button33.TabIndex = 41;
            this.button33.Text = "4";
            this.button33.UseVisualStyleBackColor = true;
            // 
            // button34
            // 
            this.button34.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button34.Location = new System.Drawing.Point(398, 97);
            this.button34.Margin = new System.Windows.Forms.Padding(2);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(21, 28);
            this.button34.TabIndex = 40;
            this.button34.Text = "3";
            this.button34.UseVisualStyleBackColor = true;
            // 
            // button35
            // 
            this.button35.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button35.Location = new System.Drawing.Point(373, 97);
            this.button35.Margin = new System.Windows.Forms.Padding(2);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(21, 28);
            this.button35.TabIndex = 39;
            this.button35.Text = "2";
            this.button35.UseVisualStyleBackColor = true;
            // 
            // button36
            // 
            this.button36.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button36.Location = new System.Drawing.Point(347, 97);
            this.button36.Margin = new System.Windows.Forms.Padding(2);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(21, 28);
            this.button36.TabIndex = 38;
            this.button36.Text = "1";
            this.button36.UseVisualStyleBackColor = true;
            // 
            // button37
            // 
            this.button37.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button37.Location = new System.Drawing.Point(551, 97);
            this.button37.Margin = new System.Windows.Forms.Padding(2);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(21, 28);
            this.button37.TabIndex = 37;
            this.button37.Text = "9";
            this.button37.UseVisualStyleBackColor = true;
            // 
            // button38
            // 
            this.button38.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button38.Location = new System.Drawing.Point(525, 140);
            this.button38.Margin = new System.Windows.Forms.Padding(2);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(21, 28);
            this.button38.TabIndex = 54;
            this.button38.Text = "17";
            this.button38.UseVisualStyleBackColor = true;
            // 
            // button39
            // 
            this.button39.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button39.Location = new System.Drawing.Point(500, 140);
            this.button39.Margin = new System.Windows.Forms.Padding(2);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(21, 28);
            this.button39.TabIndex = 53;
            this.button39.Text = "16";
            this.button39.UseVisualStyleBackColor = true;
            // 
            // button40
            // 
            this.button40.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button40.Location = new System.Drawing.Point(475, 140);
            this.button40.Margin = new System.Windows.Forms.Padding(2);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(21, 28);
            this.button40.TabIndex = 52;
            this.button40.Text = "15";
            this.button40.UseVisualStyleBackColor = true;
            // 
            // button41
            // 
            this.button41.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button41.Location = new System.Drawing.Point(449, 140);
            this.button41.Margin = new System.Windows.Forms.Padding(2);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(21, 28);
            this.button41.TabIndex = 51;
            this.button41.Text = "14";
            this.button41.UseVisualStyleBackColor = true;
            // 
            // button42
            // 
            this.button42.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button42.Location = new System.Drawing.Point(423, 140);
            this.button42.Margin = new System.Windows.Forms.Padding(2);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(21, 28);
            this.button42.TabIndex = 50;
            this.button42.Text = "13";
            this.button42.UseVisualStyleBackColor = true;
            // 
            // button43
            // 
            this.button43.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button43.Location = new System.Drawing.Point(398, 140);
            this.button43.Margin = new System.Windows.Forms.Padding(2);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(21, 28);
            this.button43.TabIndex = 49;
            this.button43.Text = "12";
            this.button43.UseVisualStyleBackColor = true;
            // 
            // button44
            // 
            this.button44.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button44.Location = new System.Drawing.Point(373, 140);
            this.button44.Margin = new System.Windows.Forms.Padding(2);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(21, 28);
            this.button44.TabIndex = 48;
            this.button44.Text = "11";
            this.button44.UseVisualStyleBackColor = true;
            // 
            // button45
            // 
            this.button45.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button45.Location = new System.Drawing.Point(347, 140);
            this.button45.Margin = new System.Windows.Forms.Padding(2);
            this.button45.Name = "button45";
            this.button45.Size = new System.Drawing.Size(21, 28);
            this.button45.TabIndex = 47;
            this.button45.Text = "10";
            this.button45.UseVisualStyleBackColor = true;
            // 
            // button46
            // 
            this.button46.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button46.Location = new System.Drawing.Point(551, 140);
            this.button46.Margin = new System.Windows.Forms.Padding(2);
            this.button46.Name = "button46";
            this.button46.Size = new System.Drawing.Size(21, 28);
            this.button46.TabIndex = 46;
            this.button46.Text = "18";
            this.button46.UseVisualStyleBackColor = true;
            // 
            // button47
            // 
            this.button47.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button47.Location = new System.Drawing.Point(525, 185);
            this.button47.Margin = new System.Windows.Forms.Padding(2);
            this.button47.Name = "button47";
            this.button47.Size = new System.Drawing.Size(21, 28);
            this.button47.TabIndex = 63;
            this.button47.Text = "26";
            this.button47.UseVisualStyleBackColor = true;
            // 
            // button48
            // 
            this.button48.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button48.Location = new System.Drawing.Point(500, 185);
            this.button48.Margin = new System.Windows.Forms.Padding(2);
            this.button48.Name = "button48";
            this.button48.Size = new System.Drawing.Size(21, 28);
            this.button48.TabIndex = 62;
            this.button48.Text = "25";
            this.button48.UseVisualStyleBackColor = true;
            this.button48.Click += new System.EventHandler(this.button48_Click);
            // 
            // button49
            // 
            this.button49.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button49.Location = new System.Drawing.Point(475, 185);
            this.button49.Margin = new System.Windows.Forms.Padding(2);
            this.button49.Name = "button49";
            this.button49.Size = new System.Drawing.Size(21, 28);
            this.button49.TabIndex = 61;
            this.button49.Text = "24";
            this.button49.UseVisualStyleBackColor = true;
            // 
            // button50
            // 
            this.button50.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button50.Location = new System.Drawing.Point(449, 185);
            this.button50.Margin = new System.Windows.Forms.Padding(2);
            this.button50.Name = "button50";
            this.button50.Size = new System.Drawing.Size(21, 28);
            this.button50.TabIndex = 60;
            this.button50.Text = "23";
            this.button50.UseVisualStyleBackColor = true;
            // 
            // button51
            // 
            this.button51.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button51.Location = new System.Drawing.Point(423, 185);
            this.button51.Margin = new System.Windows.Forms.Padding(2);
            this.button51.Name = "button51";
            this.button51.Size = new System.Drawing.Size(21, 28);
            this.button51.TabIndex = 59;
            this.button51.Text = "22";
            this.button51.UseVisualStyleBackColor = true;
            // 
            // button52
            // 
            this.button52.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button52.Location = new System.Drawing.Point(398, 185);
            this.button52.Margin = new System.Windows.Forms.Padding(2);
            this.button52.Name = "button52";
            this.button52.Size = new System.Drawing.Size(21, 28);
            this.button52.TabIndex = 58;
            this.button52.Text = "21";
            this.button52.UseVisualStyleBackColor = true;
            // 
            // button53
            // 
            this.button53.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button53.Location = new System.Drawing.Point(373, 185);
            this.button53.Margin = new System.Windows.Forms.Padding(2);
            this.button53.Name = "button53";
            this.button53.Size = new System.Drawing.Size(21, 28);
            this.button53.TabIndex = 57;
            this.button53.Text = "20";
            this.button53.UseVisualStyleBackColor = true;
            // 
            // button54
            // 
            this.button54.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button54.Location = new System.Drawing.Point(347, 185);
            this.button54.Margin = new System.Windows.Forms.Padding(2);
            this.button54.Name = "button54";
            this.button54.Size = new System.Drawing.Size(21, 28);
            this.button54.TabIndex = 56;
            this.button54.Text = "19";
            this.button54.UseVisualStyleBackColor = true;
            // 
            // button55
            // 
            this.button55.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button55.Location = new System.Drawing.Point(551, 185);
            this.button55.Margin = new System.Windows.Forms.Padding(2);
            this.button55.Name = "button55";
            this.button55.Size = new System.Drawing.Size(21, 28);
            this.button55.TabIndex = 55;
            this.button55.Text = "27";
            this.button55.UseVisualStyleBackColor = true;
            this.button55.Click += new System.EventHandler(this.button55_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(99, 197);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(81, 22);
            this.button1.TabIndex = 80;
            this.button1.Text = "бронювання";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(99, 9);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(80, 22);
            this.button2.TabIndex = 81;
            this.button2.Text = "купівля";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // calendar
            // 
            this.calendar.BackColor = System.Drawing.Color.SeaGreen;
            this.calendar.Location = new System.Drawing.Point(11, 9);
            this.calendar.Margin = new System.Windows.Forms.Padding(2);
            this.calendar.Name = "calendar";
            this.calendar.Size = new System.Drawing.Size(81, 20);
            this.calendar.TabIndex = 86;
            this.calendar.Text = "Дата";
            this.calendar.TextChanged += new System.EventHandler(this.calendar_TextChanged);
            // 
            // checkboxdate
            // 
            this.checkboxdate.BackColor = System.Drawing.Color.LightGray;
            this.checkboxdate.FormattingEnabled = true;
            this.checkboxdate.Items.AddRange(new object[] {
            "15.09.2021",
            "16.09.2021",
            "17.09.2021",
            "18.09.2021",
            "18.09.2021"});
            this.checkboxdate.Location = new System.Drawing.Point(10, 33);
            this.checkboxdate.Margin = new System.Windows.Forms.Padding(2);
            this.checkboxdate.Name = "checkboxdate";
            this.checkboxdate.Size = new System.Drawing.Size(81, 79);
            this.checkboxdate.TabIndex = 95;
            this.checkboxdate.SelectedIndexChanged += new System.EventHandler(this.checkboxdate_SelectedIndexChanged);
            // 
            // seattext
            // 
            this.seattext.BackColor = System.Drawing.Color.SeaGreen;
            this.seattext.Location = new System.Drawing.Point(11, 116);
            this.seattext.Margin = new System.Windows.Forms.Padding(2);
            this.seattext.Name = "seattext";
            this.seattext.Size = new System.Drawing.Size(81, 20);
            this.seattext.TabIndex = 97;
            this.seattext.Text = "Місця";
            this.seattext.TextChanged += new System.EventHandler(this.seattext_TextChanged);
            // 
            // checkseatbox
            // 
            this.checkseatbox.BackColor = System.Drawing.SystemColors.ControlLight;
            this.checkseatbox.FormattingEnabled = true;
            this.checkseatbox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32",
            "33",
            "34",
            "35",
            "36"});
            this.checkseatbox.Location = new System.Drawing.Point(11, 140);
            this.checkseatbox.Margin = new System.Windows.Forms.Padding(2);
            this.checkseatbox.Name = "checkseatbox";
            this.checkseatbox.Size = new System.Drawing.Size(81, 79);
            this.checkseatbox.TabIndex = 98;
            this.checkseatbox.SelectedIndexChanged += new System.EventHandler(this.checkedListBox1_SelectedIndexChanged_3);
            // 
            // mainprog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.ClientSize = new System.Drawing.Size(614, 303);
            this.Controls.Add(this.checkseatbox);
            this.Controls.Add(this.seattext);
            this.Controls.Add(this.checkboxdate);
            this.Controls.Add(this.calendar);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button47);
            this.Controls.Add(this.button48);
            this.Controls.Add(this.button49);
            this.Controls.Add(this.button50);
            this.Controls.Add(this.button51);
            this.Controls.Add(this.button52);
            this.Controls.Add(this.button53);
            this.Controls.Add(this.button54);
            this.Controls.Add(this.button55);
            this.Controls.Add(this.button38);
            this.Controls.Add(this.button39);
            this.Controls.Add(this.button40);
            this.Controls.Add(this.button41);
            this.Controls.Add(this.button42);
            this.Controls.Add(this.button43);
            this.Controls.Add(this.button44);
            this.Controls.Add(this.button45);
            this.Controls.Add(this.button46);
            this.Controls.Add(this.button29);
            this.Controls.Add(this.button30);
            this.Controls.Add(this.button31);
            this.Controls.Add(this.button32);
            this.Controls.Add(this.button33);
            this.Controls.Add(this.button34);
            this.Controls.Add(this.button35);
            this.Controls.Add(this.button36);
            this.Controls.Add(this.button37);
            this.Controls.Add(this.pictureBox1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "mainprog";
            this.Text = "головна сторінка";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.Button button45;
        private System.Windows.Forms.Button button46;
        private System.Windows.Forms.Button button47;
        private System.Windows.Forms.Button button48;
        private System.Windows.Forms.Button button49;
        private System.Windows.Forms.Button button50;
        private System.Windows.Forms.Button button51;
        private System.Windows.Forms.Button button52;
        private System.Windows.Forms.Button button53;
        private System.Windows.Forms.Button button54;
        private System.Windows.Forms.Button button55;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox calendar;
        private System.Windows.Forms.CheckedListBox checkboxdate;
        private System.Windows.Forms.TextBox seattext;
        private System.Windows.Forms.CheckedListBox checkseatbox;
    }
}