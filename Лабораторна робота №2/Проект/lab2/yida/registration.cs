﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace kino2norm
{
    public partial class registration : Form
    {
        public registration()
        {
            InitializeComponent();
        }

        private void registration_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {


            DB db = new DB();
            MySqlCommand command = new MySqlCommand("INSERT INTO `users` (`login` ,`pass` ) VALUES (@login , @pass )", db.GetConnection());
            
            command.Parameters.Add("@login", MySqlDbType.VarChar).Value = textBox1.Text;
            command.Parameters.Add("@pass", MySqlDbType.VarChar).Value = textBox2.Text;

            db.openConnection();

            if (command.ExecuteNonQuery() == 1)
                MessageBox.Show("Профіль створено");

            else
                MessageBox.Show("Помилка створення профілю");



            db.CloseConnection();



            //mainprog f1 = new mainprog();
            //f1.Show();
            //this.Hide();
        }

        private void login2_Click(object sender, EventArgs e)
        {
           LoginForm1 f1 = new LoginForm1();
            f1.Show();
            this.Hide();
        }
    }
}
